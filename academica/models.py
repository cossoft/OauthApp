from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.translation import ugettext_lazy as _
from django.utils.crypto import get_random_string
# Create your models here.
"""
# *******************************************************
# PASO 1 -------->
# Modelos academica App
"""


def path_file(instance, filename):
    """
    Función guardar archivo de imágen con nombres únicos
    """
    return 'users/profile/{0}{1}.{2}'.format(
        instance.username + '_',
        get_random_string(length=8),
        filename.split('.')[-1]
    )


class Subject(models.Model):
    """
    Subject encapsula un objeto asignatura
    :subject_id: Llave primaria del modelo
    :subject_name: nombre de la asignatura
    """
    subject_id = models.AutoField(primary_key=True)
    subject_name = models.CharField(
        null=False,
        unique=True,
        max_length=128
    )

    def __str__(self):
        """
        Función especial python para imprimir un objeto
        :return: nombre asignatura
        """
        return self.subject_name

    class Meta:
        """
        Anotaciones de nombre dentro de administrador
        y ordenamiento de objetos por defecto
        """
        verbose_name_plural = "Materia"
        ordering = ("subject_name",)


class Student(AbstractUser):
    """
    Student encapsula un objeto estudiante
    heredando del objeto AbstractUser
    usado como modelo de autenticación
    personalizado agregando atributos
    propios del objeto estudiante
    """
    genderOptions = (
        ('F', 'Mujer'),
        ('M', 'Hombre'),
        ('O', 'Otro'),
        ('R', 'Reservado')
    )
    id = models.AutoField(primary_key=True)
    student_code = models.CharField(
        _('student code'),
        null=True,
        unique=True,
        db_index=True,
        max_length=10
    )
    student_age = models.IntegerField(
        validators=[MinValueValidator(14),
                    MaxValueValidator(70)],
        null=True
    )
    student_gender = models.CharField(
        choices=genderOptions,
        default='F',
        max_length=1
    )
    student_profile = models.ImageField(
        upload_to=path_file,
        null=True
    )
    student_semester = models.IntegerField(
        validators=[MinValueValidator(1),
                    MaxValueValidator(10)],
        null=True
    )

    def __str__(self):
        name = self.first_name + ' ' + self.last_name
        code = self.student_code \
            if (self.student_code is not None and
                self.student_code != "") \
            else ""
        return code + ' ' + name \
            if (name != ' ' and code != "") \
            else code + ' ' + self.username

    class Meta:
        verbose_name_plural = "Estudiante"
        ordering = ("student_code",)


class Note(models.Model):
    """
    Note encapsula un objeto nota
    relacionado con los modelos
    estudiante y asignatura
    """
    note_id = models.AutoField(primary_key=True)
    subject = models.ForeignKey(
        Subject,
        on_delete=models.CASCADE,
        db_column='subject_id',
        related_name='subjectNote'
    )
    student = models.ForeignKey(
        Student,
        on_delete=models.CASCADE,
        db_column='id',
        related_name='subjectStudent'
    )
    note_final = models.DecimalField(
        max_digits=2,
        decimal_places=1,
        default=0,
        validators=[MinValueValidator(0),
                    MaxValueValidator(5)],
    )

    def __str__(self):
        return str(self.student) + ' ' + str(self.subject)

    class Meta:
        verbose_name_plural = "Nota"
        ordering = ("subject",)
